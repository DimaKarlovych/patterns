package patterns.behavioral.observer;

public class JobSearch {
    public static void main(String[] args) {
        JavaDeveloperJobSite javaDeveloperJobSite = new JavaDeveloperJobSite();
        javaDeveloperJobSite.addVacancy("Junior Java Developer");
        javaDeveloperJobSite.addVacancy("Senior Java Developer");

        Observer subscriber = new Subscriber("Dima");
        Observer subscriber2 = new Subscriber("Alexander");

        javaDeveloperJobSite.addObserver(subscriber);
        javaDeveloperJobSite.addObserver(subscriber2);

        javaDeveloperJobSite.addVacancy("Middle Java Developer");

        javaDeveloperJobSite.removeVacancy("Junior Java Developer");
    }
}
