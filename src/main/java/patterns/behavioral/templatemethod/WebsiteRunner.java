package patterns.behavioral.templatemethod;

public class WebsiteRunner {
    public static void main(String[] args) {
        WebsiteTemplate templateWelcome = new WelcomePage();
        WebsiteTemplate templateNews = new NewsPage();

        templateWelcome.showPage();
        System.out.println("");
        templateNews.showPage();
    }
}
