package patterns.behavioral.mediator;

public class SimpleChatRunner {
    public static void main(String[] args) {
        SimpleTextChat simpleTextChat = new SimpleTextChat();

        Admin admin = new Admin(simpleTextChat, "admin");

        User user = new SimpleUser(simpleTextChat, "user1");
        User user2 = new SimpleUser(simpleTextChat, "user2");

        simpleTextChat.setAdmin(admin);

        simpleTextChat.addUserToChat(user);
        simpleTextChat.addUserToChat(user2);

        user.sendMessage("hello");
        admin.sendMessage("I'M ADMIN!");
    }
}
