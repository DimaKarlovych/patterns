package patterns.behavioral.iterator;

public class JavaDeveloperRunner {
    public static void main(String[] args) {
        String [] skills = {"Java", "Spring", "Hibernate", "Maven", "PostreSQL"};

        JavaDeveloper javaDeveloper = new JavaDeveloper("Karlovych", skills);

        Iterator iterator = javaDeveloper.getIterator();
        System.out.println("Developer: " + javaDeveloper.getName()+" Skills: ");

        while(iterator.hasNext()) {
            System.out.print(iterator.next().toString() + " ");
        }
    }
}
