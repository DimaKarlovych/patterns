package patterns.behavioral.command;

public class Developer {
    Command insert;
    Command update;
    Command select;
    Command remove;

    public Developer(Command insert, Command update, Command select, Command remove) {
        this.insert = insert;
        this.update = update;
        this.select = select;
        this.remove = remove;
    }

    public void insertRecord() {
        insert.execute();
    }


    public void updateRecord() {
        update.execute();
    }


    public void selectRecord() {
        select.execute();
    }

    public void removeRecord() {
        remove.execute();
    }
}
