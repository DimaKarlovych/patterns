package patterns.structural.composite;

public class Project {
    public static void main(String[] args) {
        DeveloperTeam developerTeam = new DeveloperTeam();

        Developer firstJavaDeveloper = new JavaDeveloper();
        Developer secondJavaDeveloper = new JavaDeveloper();
        Developer cppDeveloper = new CppDeveloper();

        developerTeam.addDeveloper(firstJavaDeveloper);
        developerTeam.addDeveloper(secondJavaDeveloper);
        developerTeam.addDeveloper(cppDeveloper);

        developerTeam.createProject();
    }
}
