package patterns.structural.adapter;

public class SocketAdapter  {
    private EUSocket euSocket;

    public SocketAdapter(EUSocket euSocket) {
        this.euSocket = euSocket;
    }

    AmericanSocket getSocket() {
        AmericanSocket americanSocket = new AmericanSocket();
        americanSocket.setVoltage(this.euSocket.getVoltage()/2);
        return americanSocket;
    }
}
