package patterns.structural.adapter;

public class AmericanSocket {

    public AmericanSocket() {

    }

    public int getVoltage() {
        return voltage;
    }

    public void setVoltage(int voltage) {
        this.voltage = voltage;
    }

    private int voltage = 110;

    public AmericanSocket(int voltage) {
        this.voltage = voltage;
    }
}
