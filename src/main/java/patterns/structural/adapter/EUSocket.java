package patterns.structural.adapter;

public class EUSocket  {
    private int voltage = 220;

    public int getVoltage() {
        return voltage;
    }

    public void setVoltage(int voltage) {
        this.voltage = voltage;
    }

    public EUSocket(int voltage) {
        this.voltage = voltage;
    }
}
