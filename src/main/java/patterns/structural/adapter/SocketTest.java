package patterns.structural.adapter;

public class SocketTest {
    public static void main(String[] args) {
        EUSocket euSocket = new EUSocket(260);

        SocketAdapter socketAdapter = new SocketAdapter(euSocket);

        AmericanSocket americanSocket = socketAdapter.getSocket();

        System.out.println(americanSocket.getVoltage());
    }
}
