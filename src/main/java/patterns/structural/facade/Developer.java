package patterns.structural.facade;

public class Developer {
    public void doJobBeforeDeadline(BugTracker bugTracker) {
        if(bugTracker.isActiveSprint()) {
            System.out.println("Developer doing job.");
        } else {
            System.out.println("Developer is surfing Google.");
        }
    }
}
