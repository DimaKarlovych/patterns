package patterns.structural.facade;

public class WorkProcess {
    BugTracker bugTracker = new BugTracker();
    Developer developer = new Developer();
    Job job = new Job();

    public void solveProblems() {
        job.doJob();
        bugTracker.startSprint();
        developer.doJobBeforeDeadline(bugTracker);
    }
}
