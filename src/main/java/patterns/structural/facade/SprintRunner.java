package patterns.structural.facade;

public class SprintRunner {
    public static void main(String[] args) {
        WorkProcess workProcess = new WorkProcess();
        workProcess.solveProblems();
    }
}
