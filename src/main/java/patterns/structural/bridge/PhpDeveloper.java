package patterns.structural.bridge;

public class PhpDeveloper implements Developer {
    @Override
    public void writeCode() {
        System.out.println("Writing php code...");
    }
}
