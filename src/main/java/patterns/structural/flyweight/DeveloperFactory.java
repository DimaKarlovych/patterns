package patterns.structural.flyweight;

import java.util.HashMap;
import java.util.Map;

public class DeveloperFactory {
    private static final Map<String, Developer> developers = new HashMap<String, Developer>();

    public Developer getDeveloperBySpecialty(String specialty) {
        Developer developer = developers.get(specialty);

        if (developer == null) {
            switch (specialty) {
                case "java":
                    System.out.println("Hiring JavaDeveloper");
                    developer = new JavaDeveloper();
                    break;
                case "cpp":
                    System.out.println("Hiring CppDeveloper");
                    developer = new CppDeveloper();
            }
            developers.put(specialty, developer);
        }

        return developer;
    }
}
