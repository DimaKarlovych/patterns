package patterns.creational.builder;

public class EnterpriseWebsiteBuilder extends WebsiteBuilder {
    @Override
    void buildName() {
        website.setName("Enterprise website");
    }

    @Override
    void buildPlatform() {
        website.setPlatform(Platform.NON_CROSS_PLATFORM);
    }

    @Override
    void buildPrice() {
        website.setPrice(2000);
    }
}
