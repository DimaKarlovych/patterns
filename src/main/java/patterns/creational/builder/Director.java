package patterns.creational.builder;

public class Director {
    WebsiteBuilder builder;

    public void setBuilder(WebsiteBuilder builder) {
        this.builder = builder;
    }

    public Website buildWebsite() {
        builder.createWebsite();
        builder.buildName();
        builder.buildPlatform();
        builder.buildPrice();

        Website website = builder.getWebsite();

        return website;
    }
}
