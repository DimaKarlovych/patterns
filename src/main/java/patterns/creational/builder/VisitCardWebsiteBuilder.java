package patterns.creational.builder;

public class VisitCardWebsiteBuilder extends WebsiteBuilder{

    @Override
    void buildName() {
        website.setName("Visit card");
    }

    @Override
    void buildPlatform() {
        website.setPlatform(Platform.CROSS_PLATFORM);
    }

    @Override
    void buildPrice() {
        website.setPrice(1000);
    }
}
