package patterns.creational.builder;

public enum Platform {
    CROSS_PLATFORM,
    NON_CROSS_PLATFORM
}
