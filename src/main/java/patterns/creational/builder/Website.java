package patterns.creational.builder;

public class Website {
    private String name;
    private Platform platform;
    private int price;

    public void setName(String name) {
        this.name = name;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Website{" +
                "name='" + name + '\'' +
                ", platform=" + platform +
                ", price=" + price +
                '}';
    }
}
