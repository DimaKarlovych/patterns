package patterns.creational.abstractfactory;

public class ShapeFactoryImpl implements ShapeFactory {
    @Override
    public Shape getShape(String type) {
        if(type.equals("rectangle")) {
            return new Rectangle();
        } else if (type.equals("triangle")) {
            return new Triangle();
        } else if (type.equals("circle")){
            return new Circle();
        } else {
            throw new IllegalArgumentException("We can't draw this shape!!!");
        }
    }
}
