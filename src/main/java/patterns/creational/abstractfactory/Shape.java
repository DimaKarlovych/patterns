package patterns.creational.abstractfactory;

public interface Shape {
    public void draw();
}
