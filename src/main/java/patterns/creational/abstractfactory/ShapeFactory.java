package patterns.creational.abstractfactory;

public interface ShapeFactory {
    public Shape getShape(String type);
}
