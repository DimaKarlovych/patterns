package patterns.creational.abstractfactory;

public class PaperRunner {
    public static void main(String[] args) {
        ShapeFactoryImpl shapeFactory = new ShapeFactoryImpl();
        Shape shape = shapeFactory.getShape("rectangle");
        Shape shape2 = shapeFactory.getShape("triangle");
        Shape shape3 = shapeFactory.getShape("circle");

        shape.draw();
        shape2.draw();
        shape3.draw();
    }
}
