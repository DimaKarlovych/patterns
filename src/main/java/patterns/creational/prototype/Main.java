package patterns.creational.prototype;

public class Main {
    public static void main(String[] args) {
        Project project = new Project(1, "First", "Source code...");

        System.out.println(project);

        ProjectFactory projectFactory = new ProjectFactory(project);
        Project cloneProject = projectFactory.cloneProject();
        System.out.println(cloneProject);
    }
}
