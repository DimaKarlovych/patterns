package patterns.creational.singleton;

public class Bank {
    private static Bank bank;

    private String name = "PrivatBank";

    private Bank() {}

    static Bank getInstance() {
        if(bank == null) {
            bank = new Bank();
        }
        return bank;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "name='" + name + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Bank bank = Bank.getInstance();
        System.out.println(bank);

        Bank bank2 = Bank.getInstance();
        System.out.println(bank2);
    }
}
