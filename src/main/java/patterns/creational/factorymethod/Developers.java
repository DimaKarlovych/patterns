package patterns.creational.factorymethod;

public enum Developers {
    JAVA_DEVELOPER,
    CPP_DEVELOPER
}
