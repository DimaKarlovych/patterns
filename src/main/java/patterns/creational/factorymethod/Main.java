package patterns.creational.factorymethod;

public class Main {
    public static void main(String[] args) {
        DeveloperFactory developerFactory = createDeveloperFactoryBySpecialty(Developers.JAVA_DEVELOPER);
        Developer developer = developerFactory.createDeveloper();

        developer.writeCode();

    }

    static  DeveloperFactory createDeveloperFactoryBySpecialty(Developers developers) {
        if (developers == Developers.CPP_DEVELOPER) {
            return new CppDeveloperFactory();
        } else if (developers == Developers.JAVA_DEVELOPER) {
            return new JavaDeveloperFactory();
        }
        throw new IllegalArgumentException("Illegal argument exception!");
    }
}
