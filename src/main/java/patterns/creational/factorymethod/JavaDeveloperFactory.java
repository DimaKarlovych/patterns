package patterns.creational.factorymethod;

public class JavaDeveloperFactory implements  DeveloperFactory{
    public Developer createDeveloper() {
        return new JavaDeveloper();
    }
}
