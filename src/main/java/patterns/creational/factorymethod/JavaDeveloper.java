package patterns.creational.factorymethod;

public class JavaDeveloper implements Developer{
    public void writeCode() {
        System.out.println("Java code is writing...");
    }
}
