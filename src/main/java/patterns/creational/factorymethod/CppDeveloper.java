package patterns.creational.factorymethod;

public class CppDeveloper implements Developer {
    public void writeCode() {
        System.out.println("Cpp code is writing...");
    }
}